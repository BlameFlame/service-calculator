const { resolve, relative } = require('path');
const colors = require('colors/safe');
var fs = require('fs');

const file = resolve(__dirname, 'src', 'environments', 'build-info.ts');


const buildInfoSrc =
    `// IMPORTANT: THIS FILE IS AUTO GENERATED! DO NOT MANUALLY EDIT OR CHECKIN!
 const LAST_BUILD_TIME = \'${new Date().toLocaleDateString('ru-RU', { year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric' })}\';
`;

fs.writeFile(file, buildInfoSrc, function(err) {
    if (err) console.err(colors.red('Can\'t wrote build info.'));
    else console.log(colors.green(`Wrote build info to ${relative(resolve(__dirname, '..'), file)}`));
});
