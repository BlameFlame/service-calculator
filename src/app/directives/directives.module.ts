import { NgModule } from '@angular/core';
import { OnlyNumbersDirective } from './input/only-numbers.directive';


@NgModule({
    declarations: [
        OnlyNumbersDirective

    ],
    exports: [
        OnlyNumbersDirective
    ],
    imports: [
    ],
    entryComponents: [
    ],
    providers: [],
})
export class DirectivesModule { }
