import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appOnlyNumbers]'
})
export class OnlyNumbersDirective {

  private allowKeys = {
    backspace: 8,
    enter: 13,
    delete: 46,
    home: 36,
    end: 35,
    leftarrow: 37,
    rightarrow: 39,
  };

  private get currentValue(): string {
    return this.el.nativeElement.value;
  }

  private set currentValue(value: string) {
    this.el.nativeElement.value = value;
  }

  @Input()
  public maxSymbols = 10;

  constructor(private el: ElementRef) { }

  @HostListener('keydown', ['$event']) onkeydown(e: KeyboardEvent) {
    if (Object.values(this.allowKeys).indexOf(e.keyCode) !== -1 ||
      // Allow: Ctrl+A
      (e.keyCode === 65 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+C
      (e.keyCode === 67 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+X
      (e.keyCode === 88 && (e.ctrlKey || e.metaKey))) {
      // let it happen, don't do anything
      return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
    }

    if (this.currentValue.length === this.maxSymbols) {
      e.preventDefault();
      return;
    }
  }

}
