/**
* Возвращает количество тысяч из числа (123007) => 123
* @param value Число
*/
export function getThousands(value: number) {
    return Math.trunc(value / 1000);
}

/**
 * Возвращает значение до тысяч (123007) => 7
 * @param value Число
 */
export function getRemainderOfThousand(value: number) {
    const th = getThousands(value);
    const result = value - th * 1000;
    return result;
}

/**
 * Форматирует число до определенного количества символов, добавляя нули в начале (7, 3) => 007
 * @param value Число
 * @param num Количесво требуемых символов
 */
export function priceFormat(value: number, num: number) {
    let result = value.toString();
    while (result.length < num) {
        result = '0' + result;
    }
    return result;
}
