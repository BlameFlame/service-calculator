import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginComponent } from "./smart/login/login.component";
import { AdminComponent } from "./smart/admin/admin.component";
import { OrderComponent } from "./smart/order/order.component";
import { OrderHistoryComponent } from "./smart/order-history/order-history.component";
import { LegalDocumentsComponent } from "./dumb/legal-documents/legal-documents.component";
import { PrivacyPolicyComponent } from "./dumb/privacy-policy/privacy-policy.component";
import { ProcessingInformationProvidedComponent } from "./dumb/processing-information-provided/processing-information-provided.component";
import { TermsOfUseCompanyProductsComponent } from "./dumb/terms-of-use-company-products/terms-of-use-company-products.component";
import { TermsOfUseComponent } from "./dumb/terms-of-use/terms-of-use.component";
import { FaqComponent } from "./dumb/faq/faq.component";
import { AboutUsComponent } from "./dumb/about-us/about-us.component";
import { WaterProtectionComponent } from "./dumb/services/water-protection/water-protection.component";
import { FireProtectionComponent } from "./dumb/services/fire-protection/fire-protection.component";
import { SteelProtectionComponent } from "./dumb/services/steel-protection/steel-protection.component";
import { PanicButtonComponent } from "./dumb/services/panic-button/panic-button.component";
import { InsideVideoComponent } from "./dumb/services/inside-video/inside-video.component";
import { OutsideVideoComponent } from "./dumb/services/outside-video/outside-video.component";
import { AudioComponent } from "./dumb/services/audio/audio.component";
import { SoundbarComponent } from "./dumb/services/soundbar/soundbar.component";
import { AppletvComponent } from "./dumb/services/appletv/appletv.component";
import { RemoteComponent } from "./dumb/services/remote/remote.component";
import { WifiComponent } from "./dumb/services/wifi/wifi.component";

const routes: Routes = [
  { path: "admin", redirectTo: "/admin/orders", pathMatch: "full" },
  { path: "login", redirectTo: "/admin/login", pathMatch: "full" },
  { path: "", redirectTo: "/service/about-us", pathMatch: "full" },
  {
    path: "admin",
    children: [
      { path: "login", component: LoginComponent },
      { path: "orders", component: AdminComponent },
      { path: "orders/order/:id", component: OrderComponent },
      { path: "orders/order/history/:id", component: OrderHistoryComponent }
    ]
  },
  {
    path: "service",
    children: [
      {
        path: "about-us",
        component: AboutUsComponent
      },
      {
        path: "faq",
        component: FaqComponent
      },
      {
        path: "leaksprotection",
        component: WaterProtectionComponent
      },
      {
        path: "fireprotection",
        component: FireProtectionComponent
      },
      {
        path: "burglarprotection",
        component: SteelProtectionComponent
      },
      {
        path: "panicbutton",
        component: PanicButtonComponent
      },
      {
        path: "insidecameras",
        component: InsideVideoComponent
      },
      {
        path: "outsidecameras",
        component: OutsideVideoComponent
      },
      {
        path: "audio",
        component: AudioComponent
      },
      {
        path: "soundbar",
        component: SoundbarComponent
      },
      {
        path: "appletv",
        component: AppletvComponent
      },
      {
        path: "remote",
        component: RemoteComponent
      },
      {
        path: "wifi",
        component: WifiComponent
      },
      {
        path: "legal-documents",
        component: LegalDocumentsComponent
      },
      {
        path: "legal-documents/terms-of-use",
        component: TermsOfUseComponent
      },
      {
        path: "legal-documents/privacy-policy",
        component: PrivacyPolicyComponent
      },
      {
        path: "legal-documents/processing-information-provided",
        component: ProcessingInformationProvidedComponent
      },
      {
        path: "legal-documents/terms-of-use-company-products",
        component: TermsOfUseCompanyProductsComponent
      }
    ]
  },
];

@NgModule({
  exports: [RouterModule],
  imports: [
    RouterModule.forRoot(routes, { useHash: false, enableTracing: false })
  ]
})
export class AppRoutingModule {}
