import { DeviceDTO } from './deviceDTO';

export class OrderHistoryDTO {
    changeDate: string;
    author: string;
    orderHistoryDeviceses: DeviceDTO[];
}
