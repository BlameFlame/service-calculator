import { DeviceDTO } from './deviceDTO';

/**
 * DTO Услуга
 */
export class ServiceDTO {
    /**
     * Идентификатор
     */
    public id: string;
    /**
     * Имя категория
     */
    public category: string;
    /**
     * Идентификатор категории
     */
    public categoryId: number;
    /**
     * Флаг популярности категории
     */
    public popularity: boolean;
    /**
     * Имя услуги
     */
    public name: string;
    /**
     * Иконка для лендинка
     */
    public landingIcon: string;
    /**
     * Иконка для калькулятора
     */
    public calculatorIcon: string;
    /**
     * Минимальная цена для сервиса
     */
    public minimumPrice: number;
    /**
     * Описание услуги
     */
    public description: string;
    /**
     * Период гарантии в месяцах
     */
    public guaranteePeriod: number;
    /**
     * Устройства
     */
    public devices: DeviceDTO[];
    /**
     * Ссылка подробнее об услуге
     */
    public moreInfoPath: string;
}
