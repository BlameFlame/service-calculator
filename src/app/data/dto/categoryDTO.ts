import { ServiceDTO } from './serviceDTO';

export class CategoryDTO {
    public id: string;
    public name: string;
    public services: ServiceDTO[];
}
