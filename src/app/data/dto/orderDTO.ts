export class OrderDTO {
    public id: string;
    public orderDate: string;
    public orderLink: string;
    public orderStatus: string;
    public phoneNumber: string;
    public orderNumber: number;
    public totalCost: number;
    public note: string;
}
