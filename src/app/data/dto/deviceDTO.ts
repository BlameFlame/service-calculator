import { DeviceType } from '../enums/device-type';

/**
 * DTO Устройства
 */
export class DeviceDTO {
    /**
     * Идентификатор
     */
    public id: string;
    /**
     * Используется ли оно по умолчании при включении услуги
     */
    public isDefault: boolean;
    /**
     * Тип устройства
     */
    public type: DeviceType;
    /**
     * Имя устройства
     */
    public name: string;
    /**
     * Описание устройства
     */
    public description: string;
    /**
     * Количество устройств данного типа
     */
    public count: number;
    /**
     * Цена за 1 шт устройства
     */
    public price: number;
    /**
     * Необходим ли устройству смарт-хаб
     */
    public requiredSmartHub: boolean;
    /**
     * Идентификатор услуги которому принадлежит устройство
     */
    public serviceId: string;
    /**
     * Модель устройства
     */
    public model: string;
    /**
     * Период гарантии в месяцах
     */
    public guaranteePeriod: number;
    /**
     * Ссылка подробнее об устройстве
     */
    public moreInfoPath: string;
}
