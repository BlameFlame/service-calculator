/**
 * Статусы калькулятора
 */
export enum CalculatorState {
    /**
     * Активный
     */
    active = 'active',
    /**
     * Не активный
     */
    inactive = 'inactive'
}
