/**
 * Статусы заказа
 */
export enum OrderStatus {
    /**
     * Заказ
     */
    order = 'Order',
    /**
     * Нужно подумать
     */
    calculation = 'Calculation'
}
