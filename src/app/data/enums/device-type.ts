export enum DeviceType {
    smarthub = 'SmartHub',
    single = 'Single',
    default = 'Default',
    mkadOut = 'MkadOut'
}
