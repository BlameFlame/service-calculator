/**
 * Состояния запроса
 */
export enum RequestStatus {
    /**
     * Загрузка
     */
    loading = 'Loading',
    /**
     * Ошибка
     */
    error = 'Error',
    /**
     * Успешно
     */
    success = 'Success'
}
