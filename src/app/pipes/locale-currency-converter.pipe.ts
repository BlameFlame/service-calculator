import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'localeCurrencyConverter'
})
export class LocaleCurrencyConverterPipe implements PipeTransform {

  private currencyByLocale = {
    'ru-RU': 'RUB'
  };

  private defaultLocale = 'ru-RU';

  transform(value: number, locale = this.defaultLocale): string {
    return new Intl.NumberFormat(
      locale,
      {
        style: 'decimal', currency: this.currencyByLocale[locale], currencyDisplay: 'name'
      }).format(value);
  }

}
