import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'localeDateConverter'
})
export class LocaleDateConverterPipe implements PipeTransform {

  private defaultLocale = 'ru-RU';

  private defaultOptions: Intl.DateTimeFormatOptions =
    { year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric' };

  transform(value: any, locale = this.defaultLocale, dateFormatOptions = this.defaultOptions): string {
    const date = new Date(value);
    const utc = Date.UTC(
      date.getFullYear(),
      date.getMonth(),
      date.getDate(),
      date.getHours(),
      date.getMinutes(),
      date.getSeconds(),
      date.getMilliseconds()
    );
    const utcDate = new Date(utc);
    return utcDate.toLocaleDateString([locale], dateFormatOptions);
  }

}
