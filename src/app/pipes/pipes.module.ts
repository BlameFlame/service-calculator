import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LocaleDateConverterPipe } from "./locale-date-converter.pipe";
import { LocaleCurrencyConverterPipe } from "./locale-currency-converter.pipe";
import { AgeConverterPipe } from "./age-converter.pipe";

@NgModule({
  imports: [CommonModule],
  exports: [
    LocaleCurrencyConverterPipe,
    LocaleDateConverterPipe,
    AgeConverterPipe
  ],
  declarations: [
    LocaleDateConverterPipe,
    LocaleCurrencyConverterPipe,
    AgeConverterPipe
  ]
})
export class PipesModule {}
