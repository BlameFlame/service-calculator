import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { MainComponent } from "./smart/main/main.component";
import { DumbModule } from "./dumb/dumb.module";
import { ProviderModule } from "./provider/provider.module";
import { HttpClientModule } from "@angular/common/http";
import { CalculatorComponent } from "./smart/calculator/calculator.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LoginComponent } from "./smart/login/login.component";
import { AdminComponent } from "./smart/admin/admin.component";
import { OrderComponent } from "./smart/order/order.component";
import { OrderHistoryComponent } from "./smart/order-history/order-history.component";
import { OrderListComponent } from "./smart/order-list/order-list.component";
import { InterceptorsModule } from "./interceptors/interceptors.module";
import { PipesModule } from "./pipes/pipes.module";
import { LandingCalculatorComponent } from "./smart/landing-calculator/landing-calculator.component";
import { AdminCalculatorComponent } from "./smart/admin-calculator/admin-calculator.component";
import { SlickCarouselModule } from "ngx-slick-carousel";

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    CalculatorComponent,
    LoginComponent,
    AdminComponent,
    OrderComponent,
    OrderHistoryComponent,
    OrderListComponent,
    LandingCalculatorComponent,
    AdminCalculatorComponent
  ],
  imports: [
    PipesModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    DumbModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    InterceptorsModule,
    SlickCarouselModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
