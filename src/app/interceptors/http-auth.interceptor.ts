import { Injectable } from '@angular/core';
import {
    HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse
} from '@angular/common/http';

import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';
import { tap } from 'rxjs/operators';
import { Router } from '../../../node_modules/@angular/router';

/**
 * Перехватчик запросов для автоматической установки Токена безопасности
 */
@Injectable()
export class HttpAuthInterceptor implements HttpInterceptor {

    constructor(private userService: UserService, private router: Router) {

    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const authRequest = this.addTokenToRequest(request);
        return next.handle(authRequest).pipe(
            tap(
                event => {},
                error => this.handleError(error)
            )
        );
    }

    private handleError(error) {
        if (error instanceof HttpErrorResponse) {
            if (error.status === 401) {
                console.log('Пользователь не авторизован, отправляю на страницу авторизации');
                this.router.navigate(['//login']);
            }
        }
    }

    private addTokenToRequest(request: HttpRequest<any>) {
        const authRequest = request.clone({
            setHeaders: {
                Authorization: `Bearer ${this.userService.getToken()}`
            },
        });
        return authRequest;
    }

    private requestAuthCheck() {

    }
}
