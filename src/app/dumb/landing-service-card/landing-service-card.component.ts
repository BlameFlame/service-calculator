import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-landing-service-card",
  templateUrl: "./landing-service-card.component.html",
  styleUrls: ["./landing-service-card.component.scss"]
})
export class LandingServiceCardComponent implements OnInit {
  @Input()
  public img: string;

  @Input()
  public title: string;

  @Input()
  public price: number;

  @Input()
  public description: string;

  constructor() {}

  ngOnInit() {}
}
