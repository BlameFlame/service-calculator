import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-categories-of-services",
  templateUrl: "./categories-of-services.component.html",
  styleUrls: [
    "./categories-of-services.component.css",
    "./pure.categories-of-services.component.scss"
  ]
})
export class CategoriesOfServicesComponent implements OnInit {
  /**
   * Настройки слфйдера для услуг
   */
  public slidesServiceConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    centerMode: true,
    dots: true,
    mobileFirst: true,
    prevArrow:
      '<img class="prev-sl" style="width: 0px; display: none; position: absolute;">',
    nextArrow:
      '<img class="next-sl" style="width: 0px; display: none; position: absolute;">'
  };

  constructor() {}

  ngOnInit() {}
}
