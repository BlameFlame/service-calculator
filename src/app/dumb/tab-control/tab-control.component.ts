import {
  Component,
  QueryList,
  ContentChildren,
  AfterContentInit
} from "@angular/core";
import { TabItemComponent } from "../tab-item/tab-item.component";

@Component({
  selector: "app-tab-control",
  templateUrl: "./tab-control.component.html",
  styleUrls: ["./tab-control.component.scss"]
})
export class TabControlComponent implements AfterContentInit {
  @ContentChildren(TabItemComponent)
  tabItems: QueryList<TabItemComponent>;

  constructor() {}

  public ngAfterContentInit(): void {
    setTimeout(() => {
      if (this.tabItems.first) {
        this.tabItems.first.activate();
      }
    }, 100);
  }

  selectTab(item: TabItemComponent) {
    this.tabItems.forEach(tItem => (tItem.active = false));
    item.active = true;
  }
}
