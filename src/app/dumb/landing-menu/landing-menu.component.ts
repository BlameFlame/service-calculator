import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterContentChecked,
  HostListener,
  AfterContentInit,
  AfterViewInit,
  Input
} from '@angular/core';
import { CalculatorService } from '../../services/calculator.service';
import { LandingMenuService } from '../../services/landing-menu.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-landing-menu',
  templateUrl: './landing-menu.component.html',
  styleUrls: ['./landing-menu.component.scss']
})
export class LandingMenuComponent implements OnInit, AfterContentInit, AfterViewInit, AfterContentChecked {

  public mobileMode = false;

  @Input()
  public closable = false;

  public get isTop() {
    return this.menuService.isTop;
  }

  @ViewChild('menu')
  public menu: ElementRef;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
  }

  constructor(private calcService: CalculatorService, private menuService: LandingMenuService, private location: Location) { }

  ngOnInit() {
  }

  ngAfterContentInit() {
  }

  ngAfterViewInit() {
  }


  ngAfterContentChecked() {
    window.innerWidth < 1170 ? this.mobileMode = true : this.mobileMode = false;
    this.menuService.menuHeight = (this.menu.nativeElement as HTMLElement).offsetHeight;
  }

  public openCalculator() {
    this.calcService.open();
  }

  public closeClick() {
    this.location.back();
  }

}
