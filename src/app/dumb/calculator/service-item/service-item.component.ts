import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  AfterContentInit,
  ContentChildren,
  QueryList,
  OnDestroy,
  ViewChild,
  ElementRef,
  ContentChild
} from "@angular/core";
import {
  trigger,
  state,
  style,
  animate,
  transition
} from "@angular/animations";
import { DeviceItemComponent } from "../device-item/device-item.component";
import {
  getThousands,
  getRemainderOfThousand,
  priceFormat
} from "../../../stuff/converters";

@Component({
  selector: "app-service-item",
  templateUrl: "./service-item.component.html",
  styleUrls: [
    "./service-item.component.scss",
    "./pure.service-item.component.scss"
  ],
  animations: [
    trigger("sizeInOut", [
      state("in", style({ opacity: 1, height: "auto" })),
      transition("void => *", [
        style({ opacity: 0, height: "0" }),
        animate("300ms ease-in")
      ]),
      transition("* => void", [
        animate("300ms ease-out", style({ opacity: 0, height: "0" }))
      ])
    ])
  ]
})
export class ServiceItemComponent
  implements OnInit, OnDestroy, AfterContentInit {
  @Output()
  public onActivate = new EventEmitter<number>();

  @Output()
  public onDeactivate = new EventEmitter<number>();

  @Output()
  public onInit = new EventEmitter<void>();

  @Input()
  public state = "inactive";

  @Input()
  public title: string;

  @Input()
  public description: string;

  @Input()
  public icon: string;

  @Input()
  public closable = false;

  @Input()
  public accessMode = "read";

  @Input()
  public displayToggleButoon = true;

  @Input()
  public deviceListDescription = "";

  @Input()
  public moreDetailLink: string;

  /**
   * Флаг отображения сообщений о гарантии
   */
  @Input()
  public guaranteesMessagesDisplay = true;

  /**
   * Период гарантии в месяцах
   */
  @Input()
  public guaranteePeriod = 0;

  public get isReadOnly() {
    return this.accessMode === "read";
  }

  /**
   * Сервис выбран
   */
  public get isActive() {
    return this.state === "active";
  }

  @ContentChildren(DeviceItemComponent)
  deviceItems: QueryList<DeviceItemComponent> = new QueryList<
    DeviceItemComponent
  >();

  constructor() {}

  /**
   * Общая цена (Отображается при выборе количества устройств)
   */
  private get totalPrice() {
    let tPrice = 0;
    this.deviceItems.forEach(deviceItem => {
      tPrice += deviceItem.count * deviceItem.price;
    });
    return tPrice;
  }

  /**
   * Минимальная цена (отображается при не активном сервисе)
   */
  public get shadowPrice() {
    let sPrice = 0;
    this.deviceItems.forEach(deviceItem => {
      sPrice += deviceItem.isDefault ? deviceItem.price : 0;
    });
    return sPrice;
  }

  /**
   * Переключатель выбора сервиса
   */
  public toggle() {
    this.isActive ? this.deactivate() : this.activate();
    console.log(`Состояние сервиса ${this.title} изменилось на ${this.state}`);
  }

  /**
   * Выбрать сервис
   */
  public activate() {
    this.state = "active";

    /** Если при открытии карточки сервиса были установлены значения выбранных устройств */
    /** то логика с поумолчанию выбранными элементами не работает */
    if (!this.isEmptySevicesCount()) {
      this.onActivate.next(this.totalPrice);
      return;
    }

    /** Проход по всем устройствам и установка поумолчанию выбранных элементов */
    this.deviceItems.forEach(deviceItem => {
      if (!deviceItem.isDeclineMode && deviceItem.isDefault) {
        deviceItem.count = 1;
      }
      if (deviceItem.isSingle && !deviceItem.isDeclineMode) {
        deviceItem.count = 1;
      }
    });

    this.onActivate.next(this.totalPrice);
  }

  /**
   * Отменить выбор сервиса
   */
  public deactivate() {
    if (this.closable) {
      this.state = "inactive";
      this.deviceItems.forEach(deviceItem => {
        deviceItem.count = 0;
      });
      this.onDeactivate.next(this.totalPrice);
    }
  }

  public ngOnInit() {}

  public ngOnDestroy() {
    // this.deactivate();
  }

  /**
   * После загрузки всего контента выполняется проверка на выбранные устройства
   * Если устройств нет то сервис автоматически становится не выбранным
   * Если устройства есть, то сервисы автоматически выбирается
   */
  public ngAfterContentInit() {
    if (!this.hasDeclineModeItems()) {
      this.isEmptySevicesCount() ? this.deactivate() : this.activate();
    } else {
      this.activate();
    }
    this.onInit.next();
  }

  public hasDeclineModeItems() {
    let devicesCount = 0;
    this.deviceItems.forEach(deviceItem => {
      if (deviceItem.isDeclineMode) {
        devicesCount++;
      }
    });
    return devicesCount !== 0;
  }

  /**
   * Нет выбранных устройств
   */
  public isEmptySevicesCount() {
    let devicesCount = 0;
    this.deviceItems.forEach(deviceItem => {
      devicesCount += deviceItem.count;
    });
    return devicesCount === 0;
  }
}
