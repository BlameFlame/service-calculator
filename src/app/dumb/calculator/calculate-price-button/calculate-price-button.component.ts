import { Component, OnInit } from "@angular/core";
import { CalculatorService } from "../../../services/calculator.service";

@Component({
  selector: "app-calculate-price-button",
  templateUrl: "./calculate-price-button.component.html",
  styleUrls: ["./calculate-price-button.component.css"]
})
export class CalculatePriceButtonComponent implements OnInit {
  constructor(private calculatorService: CalculatorService) {}

  ngOnInit() {}

  openCalculator() {
    this.calculatorService.open();
  }
  
}
