import { Component, OnInit, Input, OnChanges, SimpleChange, Output, EventEmitter } from '@angular/core';
import { getThousands, getRemainderOfThousand, priceFormat } from '../../../stuff/converters';
import { Subject } from '../../../../../node_modules/rxjs';

@Component({
  selector: 'app-device-item',
  templateUrl: './device-item.component.html',
  styleUrls: ['./device-item.component.scss']
})
export class DeviceItemComponent implements OnInit {

  /**
   * Количетво выбранных устройств
   */
  private _count = 0;

  /**
   * Цена одного устройства
   */
  private _price = 0;

  /**
   * Общая цена (цена * количество)
   */
  private _totalPrice = 0;

  /**
   * Событие изменения общей цены
   */
  @Output()
  public onTotalPriceChanged = new EventEmitter<{ oldValue: number, newValue: number }>();

  /**
   * Событие изменения количества объктов для двухсторонней привязки данных
   */
  @Output()
  public countChange = new EventEmitter<number>();

  /**
   * Событие изменения количества объктов
   */
  @Output()
  public countChanged = new Subject<{ oldValue: number, newValue: number }>();

  /**
   * Событие изменения цены за 1 объект
   */
  @Output()
  public onPriceChanged = new EventEmitter<{ oldValue: number, newValue: number }>();

  @Input()
  public title: string;

  /**
   * Уровень доступа к изменению
   */
  @Input()
  public accessMode = 'edit';

  @Input()
  public description: string;

  @Input()
  public isDeclineMode = false;

  @Input()
  public selectMessage = '';

  @Input()
  public isSingle = false;

  /**
   * Учитывается в расчете минимальной стоимости для сервиса
   */
  @Input()
  public isDefault = false;

  /**
   * Модэль устройства
   */
  @Input()
  public model = '';

  public selected = false;

  @Input()
  public get price() { return this._price; }
  public set price(value: number) {
    if (value !== this._price) {
      this.onPriceChanged.next({ oldValue: this._price, newValue: value });
      this._price = value;
      this.totalPrice = this.price * this.count;
    }
  }

  @Input()
  public get count() { return this._count; }
  public set count(value) {
    if (value !== this._count) {
      const oldValue = this._count;
      this._count = value;
      console.log(`Количество утстройств ${this.title} изменилось c ${oldValue} на ${value}`);
      this.countChange.next(value);
      this.countChanged.next({ oldValue: oldValue, newValue: value });
      this.totalPrice = this.price * this.count;
    }
  }

  public get isReadOnly() { return this.accessMode === 'read'; }

  public get totalPrice() { return this._totalPrice; }
  public set totalPrice(value: number) {
    if (value !== this._totalPrice) {
      this.onTotalPriceChanged.next({ oldValue: this.totalPrice, newValue: value });
      this._totalPrice = value;
    }
  }

  constructor() { }

  public ngOnInit() {
  }

  public toggleSelect(event: Event) {
    const isSelected = (event.target as HTMLInputElement).checked;
    if (this.isDeclineMode) {
      isSelected ? this.decCount() : this.incCount();
    } else {
      isSelected ? this.incCount() : this.decCount();
    }
  }

  public incCountClick(event: MouseEvent) {
    this.incCount();
    event.stopPropagation();
  }

  public decCountClick(event: MouseEvent) {
    this.decCount();
    event.stopPropagation();
  }

  /**
   * Увеличить количество устройств на 1
   */
  public incCount() {
    this.count += 1;
  }

  /**
   * Уменьшить количество устройств на 1 (нельзя выбрать отрицательное количество устрйств)
   */
  public decCount() {
    this.count === 0 ? this.count = 0 : this.count -= 1;
  }

  /**
   * Возвращает количество тысяч из числа (123007) => 123
   * @param value Число
   */
  public getThousands(value: number) {
    return getThousands(value);
  }

  /**
   * Возвращает значение до тысяч (123007) => 6
   * @param value Число
   */
  public getRemainderOfThousand(value: number) {
    return getRemainderOfThousand(value);
  }

  /**
   * Форматирует число до трех символов, добавляя нули в начале (7, 3) => 007
   * @param value Число
   */
  public priceFormat(value: number) {
    return priceFormat(value, 3);
  }


}
