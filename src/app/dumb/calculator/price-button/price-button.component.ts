import { Component, OnInit, Input, Output } from "@angular/core";
import { CalculatorService } from "../../../services/calculator.service";
import { Subject } from "rxjs";

@Component({
  selector: "app-price-button",
  templateUrl: "./price-button.component.html",
  styleUrls: ["./price-button.component.css"]
})
export class PriceButtonComponent implements OnInit {
  // @Output()
  // click: Subject<Event> = new Subject<Event>();

  public get displayPrice() {
    return this.calcService.totalPrice;
  }

  /**
   * Флаг наведения на кнопку курсора
   */
  public hovered = false;

  constructor(private calcService: CalculatorService) {}

  ngOnInit() {}
}
