import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-order-service-item",
  templateUrl: "./order-service-item.component.html",
  styleUrls: ["./order-service-item.component.scss"]
})
export class OrderServiceItemComponent implements OnInit {
  @Input()
  public title = "";

  @Input()
  public totalPrice = 0;

  @Input()
  public icon = "";

  constructor() {}

  ngOnInit() {}
}
