import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-order-device-item',
  templateUrl: './order-device-item.component.html',
  styleUrls: ['./order-device-item.component.scss']
})
export class OrderDeviceItemComponent implements OnInit {

  @Input()
  public title = '';

  @Input()
  public price = 0;

  @Input()
  public count = 0;

  @Input()
  public isSingle = false;

  @Input()
  public model = '';

  @Input()
  public guaranteePeriod = 0;

  constructor() { }

  ngOnInit() {
  }

}
