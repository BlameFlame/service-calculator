import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-expand-down",
  templateUrl: "./expand-down.component.html",
  styleUrls: ["./expand-down.component.css"]
})
export class ExpandDownComponent implements OnInit {
  @Input()
  public style: string;

  constructor() {}

  ngOnInit() {}
}
