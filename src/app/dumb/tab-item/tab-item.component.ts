import { Component, OnInit, Input, OnDestroy, ElementRef, Output, EventEmitter, AfterContentChecked } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

@Component({
  selector: 'app-tab-item',
  templateUrl: './tab-item.component.html',
  styleUrls: ['./tab-item.component.scss'],
  animations: [
     trigger("sizeInOut", [
      state("in", style({ opacity: 1, height: "auto" })),
      transition("void => *", [
        style({ opacity: 0, height: "0" }),
        animate("300ms ease-in")
      ]),
      transition("* => void", [
        animate("300ms ease-out", style({ opacity: 0, height: "0" }))
      ])
    ])
  ]
})
export class TabItemComponent implements OnInit, OnDestroy, AfterContentChecked {

  private _active = false;

  @Output()
  public select: EventEmitter<void> = new EventEmitter<void>();

  @Output()
  public afterContentChecked: EventEmitter<void> = new EventEmitter<void>();

  @Input()
  public header: string;

  @Input()
  public set active(value) {
    this._active = value;
    this.select.emit();
  }

  public get active() {
    return this._active;
  }

  constructor(public tabItem: ElementRef) {
    this.active = false;
  }

  ngOnInit() {
  }

  ngOnDestroy() {

  }

  ngAfterContentChecked(): void {
    this.afterContentChecked.emit();
  }

  public activate() {
    this.active = true;
  }

}
