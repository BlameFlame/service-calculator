import { Component, OnInit, ElementRef, HostListener } from '@angular/core';
import { LandingMenuService } from '../../services/landing-menu.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {

  public get menuHeight() {
    return this.menuService.menuHeight;
  }

  @HostListener('scroll', ['$event']) onscroll(event) {
    const element = event.srcElement as HTMLElement;
    element.scrollTop === 0 ? this.menuService.isTop = true : this.menuService.isTop = false;
  }

  constructor(private elementRef: ElementRef, private menuService: LandingMenuService) {
    menuService.scrollSubject.subscribe(toElement => this.scroll(toElement));
  }

  ngOnInit() {
  }

  public scroll(toElement) {
    const item = toElement;
    const wrapper = this.elementRef.nativeElement;
    const count = item.offsetTop - wrapper.scrollTop - this.menuHeight;
    wrapper.scrollBy({ top: count, left: 0, behavior: 'smooth' });
  }

}
