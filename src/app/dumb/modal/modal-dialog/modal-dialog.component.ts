import { Component, OnInit, Input, ElementRef, OnDestroy, HostBinding, Output } from '@angular/core';
import { ModalService } from '../../../services/modal.service';
import { Subject } from 'rxjs';
import { v4 as uuid } from 'uuid';

@Component({
  selector: 'app-modal-dialog',
  templateUrl: './modal-dialog.component.html',
  styleUrls: ['./modal-dialog.component.scss']
})
export class ModalDialogComponent implements OnInit, OnDestroy {

  /**
  * Статус состояния прокрутки страницы под калькулятором
  */
  private bodyOverflowDefStyle = 'auto';

  private element: any;

  @HostBinding('class.open') isOpen = false;


  @Input()
  public closable = true;

  @Input()
  public id: string = uuid();

  @Output()
  public afterClosed: Subject<void> = new Subject<void>();

  constructor(private modalService: ModalService, private el: ElementRef) {
    this.element = el.nativeElement;
  }

  public ngOnInit() {
    const modal = this;

    // ensure id attribute exists
    if (!this.id) {
      console.error('Modal must have an id');
      return;
    }

    // move element to bottom of page (just before </body>) so it can be displayed above everything else
    // document.body.appendChild(this.element);


    // add self (this modal instance) to the modal service so it's accessible from controllers
    this.modalService.add(this);
  }

  public ngOnDestroy() {
    this.modalService.remove(this.id);
    this.element.remove();
    this.afterClosed.next();
  }

  // open modal
  public open(): void {
    // document.body.appendChild(this.element);
    // console.log(this.element);
    document.body.style.overflow = 'hidden';
    this.isOpen = true;
  }

  // close modal
  public close(): void {
    this.afterClosed.next();
    // document.body.removeChild(this.element);
    document.body.style.overflow = this.bodyOverflowDefStyle;
    this.isOpen = false;
  }

}
