import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  Renderer2,
  forwardRef,
  HostBinding,
  HostListener,
  ElementRef
} from '@angular/core';
import {
  ControlValueAccessor,
  NG_VALUE_ACCESSOR, NG_VALIDATORS, Validator, AbstractControl, ValidationErrors, FormControl
} from '@angular/forms';


export function validateFullNumber(c: FormControl) {
  const fullLen = 10;
  const phoneNum = c.value;

  const currLen = (phoneNum as string).length;

  const err = {
    fullNumber: {
      currentLength: currLen,
      fullLength: fullLen
    }
  };

  // console.log(phoneNum);
  console.log(c);
  console.log(c.invalid);
  console.log(c.valid);
  console.log(c.status);

  const error = currLen === fullLen ? null : err;
  console.log(error);
  return error;
}

export const EPANDED_TEXTAREA_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => InputPhoneNumberComponent),
  multi: true,
};

export const EPANDED_TEXTAREA_VALIDATE_ACCESSOR: any = {
  provide: NG_VALIDATORS,
  useExisting: forwardRef(() => InputPhoneNumberComponent),
  multi: true,
};

@Component({
  selector: 'app-input-phone-number',
  templateUrl: './input-phone-number.component.html',
  styleUrls: ['./input-phone-number.component.scss'],
  providers: [
    EPANDED_TEXTAREA_VALUE_ACCESSOR,
    EPANDED_TEXTAREA_VALIDATE_ACCESSOR
  ],
})
export class InputPhoneNumberComponent implements ControlValueAccessor, Validator {

  @ViewChild('textarea') textarea;

  private _partNumber = '';

  public mask = [
    '(',
    /[1-9]/, /\d/, /\d/,
    ')',
    ' ',
    /\d/, /\d/, /\d/,
    '-',
    /\d/, /\d/,
    '-',
    /\d/, /\d/
  ];

  private set data(value) {
    this.partNumber = value;
  }
  private get data() {
    return this.partNumber;
  }

  public set partNumber(value) {
    this._partNumber = value;
  }

  public get partNumber() {
    // return this._partNumber.replace(/\D+7|\D+/g, '');
    return this._partNumber.replace(/\D+/g, '');
  }


  // tslint:disable-next-line:no-input-rename
  @HostBinding('class')
  @Input('class')
  public classList = '';

  @ViewChild('input')
  public inputNumber: ElementRef;

  @Output()
  modelChange: EventEmitter<number> = new EventEmitter();

  @Input()
  public startNumber = '+7';

  @Input()
  public afterNumbreLenght = 10;

  @Input()
  public placeholder = '';

  public get isFull() {
    return this.partNumber.length === this.afterNumbreLenght;
  }

  private test = false;

  // the method set in registerOnChange, it is just
  // a placeholder for a method that takes one parameter,
  // we use it to emit changes back to the form
  private propagateChange = (_: any) => { };

  @HostListener('click', ['$event'])
  public onClick(e) {
    if (this.inputNumber) {
      this.inputNumber.nativeElement.focus();
    }
  }


  constructor(private renderer: Renderer2) { }



  validate(c: FormControl): ValidationErrors {
    if (!this.isFull) {
      return { isFull: false };
    }
    else { return null; }
  }

  // registerOnValidatorChange?(fn: () => void): void {
  // }


  writeValue(obj: any): void {
    if (obj) {
      this.data = obj;
    }
  }



  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState?(isDisabled: boolean): void {
    const div = this.textarea.nativeElement;
    const action = isDisabled ? 'addClass' : 'removeClass';
    this.renderer[action](div, 'disabled');
  }

  onChange(event: KeyboardEvent) {
    // this.test = !this.test;
    this.propagateChange(this.data);
  }

  // public validate(c: FormControl) {
  //   return (!this.parseError) ? null : {
  //     notFull: {
  //       valid: false,
  //     },
  //   };
  // }


}
