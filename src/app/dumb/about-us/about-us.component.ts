import { Component, OnInit, OnDestroy } from '@angular/core';
import { LandingMenuService } from '../../services/landing-menu.service';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: [
    './about-us.component.scss'
  ]
})
export class AboutUsComponent implements OnInit, OnDestroy {




  /**
   * Настройки слфйдера для историй из жизни
   */
  public slidesHistoryConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    mobileFirst: true,
    arrows: true,
    dots: false,

    // TODO Доделать стрелчки
    responsive: [
      {
        breakpoint: 1116,
        settings: {
          prevArrow: '<img class="prev-sl" style="cursor: pointer; position: absolute; left: -50px; top: 50%; margin-top: -35px; z-index: 5; width: 30px; height: 70px;"  src="/assets/img/left.svg">',
          nextArrow: '<img class="next-sl" style="cursor: pointer; position: absolute; right: -50px; top: 50%; margin-top: -35px; z-index: 5; width: 30px; height: 70px;"  src="/assets/img/right.svg">',
        }
      },
      {
        breakpoint: 300,
        settings: {
          prevArrow: '<img class="prev-sl" style="cursor: pointer; position: absolute; left: 0px; top: 28%; margin-top: -35px; z-index: 5; width: 30px; height: 70px;"  src="/assets/img/left.svg">',
          nextArrow: '<img class="next-sl" style="cursor: pointer; position: absolute; right: 0px; top: 28%; margin-top: -35px; z-index: 5; width: 30px; height: 70px;"  src="/assets/img/right.svg">',
        }
      }
    ]
  };


  constructor(private menuService: LandingMenuService) {
  }

  scroll(el) {
    this.menuService.scroll(el);
  }

  public ngOnInit(): void {
  }

  public ngOnDestroy() {
  }

}
