import { Component, ElementRef } from "@angular/core";
import { CalculatorService } from "../../services/calculator.service";
import { CalculatorAnalyticService } from "../../services/analytics/calculator-analytic.service";

@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.scss"]
})
export class FooterComponent {
  public nativeElement: HTMLElement;

  constructor(
    public element: ElementRef,
    private calculatorService: CalculatorService,
    private calculatorAnalytic: CalculatorAnalyticService
  ) {
    this.nativeElement = element.nativeElement;
  }

  public openCalculator() {
    this.calculatorService.open();
  }

  public waClick(){
    this.calculatorAnalytic.waLinkClick();
  }

  public tgClick(){
    this.calculatorAnalytic.telegramLinkClick();
  }

  public msClick(){
    this.calculatorAnalytic.messengerLinkClick();
  }

  public vkClick(){
    this.calculatorAnalytic.vkLinkClick();
  }

  public vbClick(){
    this.calculatorAnalytic.viberLinkClick();
  }
}
