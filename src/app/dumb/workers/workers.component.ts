import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-workers',
  templateUrl: './workers.component.html',
  styleUrls: ['./workers.component.css']
})
export class WorkersComponent implements OnInit {

  /**
  * Настройки слфйдера для слуг
  */
  public slidesWorkersConfig = {
    prevArrow: '<img class="prev-sl" style="cursor: pointer; position: absolute; left: -50px; top: 50%; margin-top: -35px; z-index: 5; width: 30px; height: 70px;"  src="/assets/img/left.svg">',
    nextArrow: '<img class="next-sl" style="cursor: pointer; position: absolute; right: -50px; top: 50%; margin-top: -35px; z-index: 5; width: 30px; height: 70px;"  src="/assets/img/right.svg">',
    slidesToShow: 3,
    slidesToScroll: 1,
    centerPadding: '0',
    centerMode: true,
    responsive: [
      {
        breakpoint: 870,
        settings: {
          prevArrow: '<img class="prev-sl" style="cursor: pointer; position: absolute; left: 0px; top: 50%; margin-top: -35px; z-index: 5; width: 30px; height: 70px;"  src="/assets/img/left.svg">',
          nextArrow: '<img class="next-sl" style="cursor: pointer; position: absolute; right: 0px; top: 50%; margin-top: -35px; z-index: 5; width: 30px; height: 70px;"  src="/assets/img/right.svg">',
          slidesToShow: 2
        }
      }, {
        breakpoint: 640,
        settings: {
          prevArrow: '<img class="prev-sl" style="cursor: pointer; position: absolute; left: 0px; top: 50%; margin-top: -35px; z-index: 5; width: 30px; height: 70px;"  src="/assets/img/left.svg">',
          nextArrow: '<img class="next-sl" style="cursor: pointer; position: absolute; right: 0px; top: 50%; margin-top: -35px; z-index: 5; width: 30px; height: 70px;"  src="/assets/img/right.svg">',
          slidesToShow: 1
        }
      }
    ]
  };

  constructor() { }

  ngOnInit() {
  }

}
