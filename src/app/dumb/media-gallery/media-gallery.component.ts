import { Component, OnInit, Input, AfterContentChecked, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-media-gallery',
  templateUrl: './media-gallery.component.html',
  styleUrls: ['./media-gallery.component.css']
})
export class MediaGalleryComponent implements OnInit {

  /**
  * Настройки слфйдера для гелереи
  */
  public slidesGalleryConfig = {
    prevArrow: '<img class="prev-sl" style="cursor: pointer; position: absolute; left: 0px; top: 50%; margin-top: -35px; z-index: 5; width: 30px; height: 70px;"  src="/assets/img/left.svg">',
    nextArrow: '<img class="next-sl" style="cursor: pointer; position: absolute; right: 0px; top: 50%; margin-top: -35px; z-index: 5; width: 30px; height: 70px;"  src="/assets/img/right.svg">',
    slidesToShow: 1,
    slidesToScroll: 1,
    centerPadding: '0px',
    centerMode: true,
    centerDisplay: 'flex;'
  };

  @Input()
  public items: { src: string, type: string }[] = [];

  @ViewChild('modal') modal;

  constructor(public sanitizer: DomSanitizer) { }

  public open() {
    this.modal.open();
  }

  ngOnInit() {
  }


}
