import { Component, OnInit, Input } from "@angular/core";
import { trigger, state, style, transition, animate } from "@angular/animations";

@Component({
  selector: "app-accordion",
  templateUrl: "./accordion.component.html",
  styleUrls: ["./accordion.component.css"],
  animations: [
    trigger("sizeInOut", [
      state("in", style({ opacity: 1, height: "auto" })),
      transition("void => *", [
        style({ opacity: 0, height: "0" }),
        animate("300ms ease-in")
      ]),
      transition("* => void", [
        animate("300ms ease-out", style({ opacity: 0, height: "0" }))
      ])
    ])
  ]
})
export class AccordionComponent implements OnInit {
  @Input()
  public title: string;

  public open = false;

  constructor() {}

  ngOnInit() {}

  public toggle() {
    this.open = !this.open;
  }
}
