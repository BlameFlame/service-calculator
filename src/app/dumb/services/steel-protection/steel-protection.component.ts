import { Component, OnInit } from "@angular/core";
import { LandingMenuService } from "../../../services/landing-menu.service";

@Component({
  selector: "app-steel-protection",
  templateUrl: "./steel-protection.component.html",
  styleUrls: ["./steel-protection.component.scss"]
})
export class SteelProtectionComponent implements OnInit {
  public items = [
    { src: "../assets/img/service_img/motion1.jpg", type: "image" },
    { src: "../assets/img/service_img/motion2.jpg", type: "image" },
    { src: "../assets/img/service_img/ajax_app.jpg", type: "image" },
    { src: "../assets/img/service_img/ajax_hub.jpg", type: "image" },
    // { src: "https://www.youtube.com/embed/hrwYPvWMIJw", type: "video" }
    { src: "https://www.youtube.com/embed/hrwYPvWMIJw", type: "video" }
  ];

  constructor(private menuService: LandingMenuService) {}

  ngOnInit() {}

  scroll(elenent) {
    this.menuService.scroll(elenent);
  }
  ƒ;
}
