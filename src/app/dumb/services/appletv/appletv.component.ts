import { Component, OnInit } from '@angular/core';
import { LandingMenuService } from '../../../services/landing-menu.service';

@Component({
  selector: 'app-appletv',
  templateUrl: './appletv.component.html',
  styleUrls: ['./appletv.component.scss']
})
export class AppletvComponent implements OnInit {

  constructor(private menuService: LandingMenuService) { 
  }

  ngOnInit() {
  }

  scroll(elenent) {
    this.menuService.scroll(elenent);
  }

}
