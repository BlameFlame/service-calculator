import { Component, OnInit } from "@angular/core";
import { LandingMenuService } from "../../../services/landing-menu.service";

@Component({
  selector: "app-audio",
  templateUrl: "./audio.component.html",
  styleUrls: ["./audio.component.scss"]
})
export class AudioComponent implements OnInit {
  public items = [
    { src: "../assets/img/service_img/yamaha_sound1.jpg", type: "image" },
    { src: "../assets/img/service_img/yamaha_sound2.jpg", type: "image" },
    { src: "../assets/img/service_img/yamaha_sound3.jpg", type: "image" },
    { src: "https://www.youtube.com/embed/oPwrnAXta9o", type: "video" }
  ];
  constructor(private menuService: LandingMenuService) {}

  ngOnInit() {}

  scroll(elenent) {
    this.menuService.scroll(elenent);
  }
}
