import { Component, OnInit } from "@angular/core";
import { LandingMenuService } from "../../../services/landing-menu.service";

@Component({
  selector: "app-water-protection",
  templateUrl: "./water-protection.component.html",
  styleUrls: ["./water-protection.component.css"]
})
export class WaterProtectionComponent implements OnInit {
  /**
   * Настройки слфйдера для услуг
   */
  public slidesServiceConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    centerMode: true,
    dots: true,
    mobileFirst: true
  };

  public items = [
    { src: "../assets/img/service_img/leaks1.jpg", type: "image" },
    { src: "../assets/img/service_img/leaks2.jpg", type: "image" },
    { src: "../assets/img/service_img/ajax_hub.jpg", type: "image" },
    { src: "../assets/img/service_img/ajax_app.jpg", type: "image" },
    { src: "https://www.youtube.com/embed/hrwYPvWMIJw", type: "video" }
  ];

  constructor(private menuService: LandingMenuService) {}

  ngOnInit() {}

  scroll(elenent) {
    this.menuService.scroll(elenent);
  }
}
