import { Component, OnInit } from "@angular/core";
import { LandingMenuService } from "../../../services/landing-menu.service";

@Component({
  selector: "app-wifi",
  templateUrl: "./wifi.component.html",
  styleUrls: ["./wifi.component.css"]
})
export class WifiComponent implements OnInit {
  public items = [
    { src: "../assets/img/service_img/zyxel1.jpg", type: "image" },
    { src: "../assets/img/service_img/zyxel2.jpg", type: "image" },
    { src: "../assets/img/service_img/zyxel3.jpg", type: "image" },
    { src: "../assets/img/service_img/zyxel4.jpg", type: "image" }
  ];

  constructor(private menuService: LandingMenuService) {}

  ngOnInit() {}

  scroll(elenent) {
    this.menuService.scroll(elenent);
  }
}
