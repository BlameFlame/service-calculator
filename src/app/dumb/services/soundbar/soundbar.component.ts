import { Component, OnInit } from "@angular/core";
import { LandingMenuService } from "../../../services/landing-menu.service";

@Component({
  selector: "app-soundbar",
  templateUrl: "./soundbar.component.html",
  styleUrls: ["./soundbar.component.scss"]
})
export class SoundbarComponent implements OnInit {
  public items = [
    { src: "../assets/img/service_img/soundbar1.jpg", type: "image" },
    { src: "../assets/img/service_img/soundbar2.jpg", type: "image" },
    { src: "../assets/img/service_img/soundbar3.jpg", type: "image" },
    { src: "https://www.youtube.com/embed/GY5XsHlM6o0", type: "video" }
  ];
  constructor(private menuService: LandingMenuService) {}

  ngOnInit() {}

  scroll(elenent) {
    this.menuService.scroll(elenent);
  }
}
