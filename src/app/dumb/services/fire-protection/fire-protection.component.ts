import { Component, OnInit } from "@angular/core";
import { LandingMenuService } from "../../../services/landing-menu.service";

@Component({
  selector: "app-fire-protection",
  templateUrl: "./fire-protection.component.html",
  styleUrls: ["./fire-protection.component.scss"]
})
export class FireProtectionComponent implements OnInit {
  public items = [
    { src: "../assets/img/service_img/fire1.jpg", type: "image" },
    { src: "../assets/img/service_img/fire2.jpg", type: "image" },
    { src: "../assets/img/service_img/ajax_hub.jpg", type: "image" },
    { src: "../assets/img/service_img/ajax_app.jpg", type: "image" },
    { src: "https://www.youtube.com/embed/hrwYPvWMIJw", type: "video" }
  ];

  constructor(private menuService: LandingMenuService) {}

  ngOnInit() {}

  scroll(elenent) {
    this.menuService.scroll(elenent);
  }
}
