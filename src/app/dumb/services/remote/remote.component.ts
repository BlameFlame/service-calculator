import { Component, OnInit } from "@angular/core";
import { LandingMenuService } from "../../../services/landing-menu.service";

@Component({
  selector: "app-remote",
  templateUrl: "./remote.component.html",
  styleUrls: ["./remote.component.scss"]
})
export class RemoteComponent implements OnInit {
  public items = [
    { src: "../assets/img/service_img/orvibo1.jpg", type: "image" },
    { src: "../assets/img/service_img/orvibo2.jpg", type: "image" },
    { src: "../assets/img/service_img/orvibo3.jpg", type: "image" },
    { src: "https://www.youtube.com/embed/Cjxs_cR34CM", type: "video" }
  ];

  constructor(private menuService: LandingMenuService) {}

  ngOnInit() {}

  scroll(elenent) {
    this.menuService.scroll(elenent);
  }
}
