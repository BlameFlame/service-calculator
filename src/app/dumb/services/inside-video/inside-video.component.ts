import { Component, OnInit } from "@angular/core";
import { LandingMenuService } from "../../../services/landing-menu.service";

@Component({
  selector: "app-inside-video",
  templateUrl: "./inside-video.component.html",
  styleUrls: ["./inside-video.component.scss"]
})
export class InsideVideoComponent implements OnInit {
  public items = [
    { src: "../assets/img/service_img/inside_video1.jpg", type: "image" },
    { src: "../assets/img/service_img/inside_video2.jpg", type: "image" },
    { src: "https://www.youtube.com/embed/cXv-LHoNa8Y", type: "video" },
    { src: "https://www.youtube.com/embed/5NXwrjODaPw", type: "video" },
    { src: "https://www.youtube.com/embed/ppDVkNco21k", type: "video" }
  ];

  constructor(private menuService: LandingMenuService) {}

  ngOnInit() {}

  scroll(elenent) {
    this.menuService.scroll(elenent);
  }
}
