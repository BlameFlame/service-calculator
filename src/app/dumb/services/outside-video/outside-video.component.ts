import { Component, OnInit } from "@angular/core";
import { LandingMenuService } from "../../../services/landing-menu.service";

@Component({
  selector: "app-outside-video",
  templateUrl: "./outside-video.component.html",
  styleUrls: ["./outside-video.component.scss"]
})
export class OutsideVideoComponent implements OnInit {
  public items = [
    { src: "../assets/img/service_img/outside_video1.jpg", type: "image" },
    { src: "../assets/img/service_img/outside_video2.jpg", type: "image" },
    { src: "../assets/img/service_img/outside_video3.jpg", type: "image" },
    { src: "https://www.youtube.com/embed/61cEzeb_0Io", type: "video" }
  ];

  constructor(private menuService: LandingMenuService) {}

  ngOnInit() {}

  scroll(elenent) {
    this.menuService.scroll(elenent);
  }
}
