import { Component, OnInit } from "@angular/core";
import { LandingMenuService } from "../../../services/landing-menu.service";

@Component({
  selector: "app-panic-button",
  templateUrl: "./panic-button.component.html",
  styleUrls: ["./panic-button.component.scss"]
})
export class PanicButtonComponent implements OnInit {
  public items = [
    { src: "../assets/img/service_img/panic_button1.jpg", type: "image" },
    { src: "../assets/img/service_img/panic_button2.jpg", type: "image" },
    { src: "../assets/img/service_img/ajax_app.jpg", type: "image" },
    { src: "../assets/img/service_img/ajax_app.jpg", type: "image" },
    { src: "https://www.youtube.com/embed/hrwYPvWMIJw", type: "video" }
  ];

  constructor(private menuService: LandingMenuService) {}

  ngOnInit() {}

  scroll(elenent) {
    this.menuService.scroll(elenent);
  }
}
