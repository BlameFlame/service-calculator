import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TabControlComponent } from './tab-control/tab-control.component';
import { TabItemComponent } from './tab-item/tab-item.component';
import { ServiceItemComponent } from './calculator/service-item/service-item.component';
import { DeviceItemComponent } from './calculator/device-item/device-item.component';
import { InputPhoneNumberComponent } from './input/input-phone-number/input-phone-number.component';
import { DirectivesModule } from '../directives/directives.module';
import { FormsModule } from '@angular/forms';
import { OrderServiceItemComponent } from './order/order-service-item/order-service-item.component';
import { OrderDeviceItemComponent } from './order/order-device-item/order-device-item.component';
import { ModalDialogComponent } from './modal/modal-dialog/modal-dialog.component';
import { SpinnerComponent } from './loading/spinner/spinner.component';
import { FooterComponent } from './footer/footer.component';
import { PriceButtonComponent } from './calculator/price-button/price-button.component';
import { PipesModule } from '../pipes/pipes.module';
import { TextMaskModule } from 'angular2-text-mask';
import { RouterModule } from '@angular/router';
import { TermsOfUseComponent } from './terms-of-use/terms-of-use.component';
import { LegalDocumentsComponent } from './legal-documents/legal-documents.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { ProcessingInformationProvidedComponent } from './processing-information-provided/processing-information-provided.component';
import { TermsOfUseCompanyProductsComponent } from './terms-of-use-company-products/terms-of-use-company-products.component';
import { ButtonComponent } from './button/button.component';
import { CartComponent } from './icons/cart/cart.component';
import { FaqComponent } from './faq/faq.component';
import { AccordionComponent } from './accordion/accordion.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { WorkersComponent } from './workers/workers.component';
import { LandingMenuComponent } from './landing-menu/landing-menu.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { WaterProtectionComponent } from './services/water-protection/water-protection.component';
import { CategoriesOfServicesComponent } from './categories-of-services/categories-of-services.component';
import { MediaGalleryComponent } from './media-gallery/media-gallery.component';
import { FireProtectionComponent } from './services/fire-protection/fire-protection.component';
import { CalculatePriceButtonComponent } from './calculator/calculate-price-button/calculate-price-button.component';
import { SteelProtectionComponent } from './services/steel-protection/steel-protection.component';
import { PanicButtonComponent } from './services/panic-button/panic-button.component';
import { InsideVideoComponent } from './services/inside-video/inside-video.component';
import { OutsideVideoComponent } from './services/outside-video/outside-video.component';
import { AudioComponent } from './services/audio/audio.component';
import { SoundbarComponent } from './services/soundbar/soundbar.component';
import { AppletvComponent } from './services/appletv/appletv.component';
import { RemoteComponent } from './services/remote/remote.component';
import { WifiComponent } from './services/wifi/wifi.component';
import { LandingServiceCardComponent } from './landing-service-card/landing-service-card.component';
import { ExpandDownComponent } from './icons/expand-down/expand-down.component';

@NgModule({
    declarations: [
        TabControlComponent,
        TabItemComponent,
        ServiceItemComponent,
        DeviceItemComponent,
        InputPhoneNumberComponent,
        OrderServiceItemComponent,
        OrderDeviceItemComponent,
        ModalDialogComponent,
        SpinnerComponent,
        FooterComponent,
        PriceButtonComponent,
        TermsOfUseComponent,
        LegalDocumentsComponent,
        PrivacyPolicyComponent,
        ProcessingInformationProvidedComponent,
        TermsOfUseCompanyProductsComponent,
        ButtonComponent,
        CartComponent,
        FaqComponent,
        AccordionComponent,
        WorkersComponent,
        LandingMenuComponent,
        LandingPageComponent,
        AboutUsComponent,
        WaterProtectionComponent,
        CategoriesOfServicesComponent,
        MediaGalleryComponent,
        FireProtectionComponent,
        CalculatePriceButtonComponent,
        SteelProtectionComponent,
        PanicButtonComponent,
        InsideVideoComponent,
        OutsideVideoComponent,
        AudioComponent,
        SoundbarComponent,
        AppletvComponent,
        RemoteComponent,
        WifiComponent,
        LandingServiceCardComponent,
        ExpandDownComponent,
    ],
    exports: [
        TabControlComponent,
        TabItemComponent,
        ServiceItemComponent,
        DeviceItemComponent,
        InputPhoneNumberComponent,
        OrderServiceItemComponent,
        OrderDeviceItemComponent,
        ModalDialogComponent,
        SpinnerComponent,
        FooterComponent,
        PriceButtonComponent,
        ButtonComponent,
        AccordionComponent,
        WorkersComponent,
        LandingMenuComponent,
        LandingPageComponent,
        AboutUsComponent
    ],
    imports: [
        BrowserModule,
        DirectivesModule,
        FormsModule,
        PipesModule,
        TextMaskModule,
        RouterModule,
        SlickCarouselModule,
    ],
    entryComponents: [
        TabItemComponent,
        ModalDialogComponent,
    ],
    providers: [],
})
export class DumbModule { }
