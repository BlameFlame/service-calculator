import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private TOKEN_KEY = 'token';

  constructor() { }

  public setToken(token: string) {
    localStorage.setItem(this.TOKEN_KEY, token);
    // console.log('setted token', token);
  }

  public getToken() {
    // console.log('returned token', localStorage.getItem(this.TOKEN_KEY));
    return localStorage.getItem(this.TOKEN_KEY);
  }
}
