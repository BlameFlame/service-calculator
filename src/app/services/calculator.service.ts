import { Injectable } from '@angular/core';
import { ServiceDTO } from '../data/dto/serviceDTO';
import { Subject } from 'rxjs';
import { CategoryDTO } from '../data/dto/categoryDTO';
import { DeviceDTO } from '../data/dto/deviceDTO';
import { OrderStatus } from '../data/enums/order-status';
import { CalculatorState } from '../data/enums/claculator-state';

/**
 * Сервис для глобального калькулятора
 */
@Injectable({
  providedIn: 'root'
})
export class CalculatorService {

  /**
   * Статус состояния прокрутки страницы под калькулятором
   */
  private bodyOverflowDefStyle = 'auto';

  private _state: CalculatorState = CalculatorState.inactive;
  private _totalPrice = 0;

  /**
   * Статус калькулятора
   */
  public get state() {
    return this._state;
  }

  public set state(value) {
    if (this._state !== value) {
      this._state = value;
      this.stateChange.next(value);
    }
  }

  /**
   * Событие инициализации данных глобального калькулятора
   */
  public onInit: Subject<{ categories: CategoryDTO[], services: ServiceDTO[], devices: DeviceDTO[] }> =
    new Subject<{ categories: CategoryDTO[], services: ServiceDTO[], devices: DeviceDTO[] }>();

  /**
   * Событие изменения статуса калькулятора
   */
  public stateChange: Subject<CalculatorState> = new Subject<CalculatorState>();

  /**
   * Событие изменения статуса калькулятора
   */
  public totalPriceChange: Subject<number> = new Subject<number>();

  // /**
  //  * Событие изменения статуса калькулятора
  //  */
  // public onDevicesClear: Subject<CalculatorState> = new Subject<CalculatorState>();

  public orderStatus: OrderStatus = OrderStatus.order;
  public services: ServiceDTO[] = [];
  public categories: CategoryDTO[] = [];

  public get devices() {
    const devices: DeviceDTO[] = [];
    this.services.forEach(service => {
      service.devices.forEach(device => {
        devices.push(device);
      });
    });
    return devices;
  }

  public get totalPrice() {
    return this._totalPrice;
  }

  public set totalPrice(value) {
    this._totalPrice = value;
    this.totalPriceChange.next(value);
  }

  public phoneNumber = '';

  constructor() {
    this.bodyOverflowDefStyle = document.body.style.overflow;
  }

  /**
   * Переключатель состояния калькулятора
   */
  public toggleState() {
    this.state === 'active' ? this.close() : this.open();
  }
  /**
   * Открыть калькулятор
   */
  public open() {
    document.body.style.overflow = 'hidden';
    this.state = CalculatorState.active;
  }
  /**
   * Закрыть калькулятор
   */
  public close() {
    document.body.style.overflow = this.bodyOverflowDefStyle;
    this.state = CalculatorState.inactive;
  }

  /**
   * Очистить все данные калькулятора
   */
  public clear() {
    this.devices.forEach(d => {
      d.count = 0;
    });
    this.totalPrice = 0;
    this.onInit.next({ categories: this.categories, services: this.services, devices: this.devices });
  }

  public getDevicesWithValue() {
    const devices = this.devices.filter(device => device.count > 0);
    return devices;
  }

  public initServices(services: ServiceDTO[]) {
    this.services = services;
    this.initCategories(services);
    this.onInit.next({ categories: this.categories, services: services, devices: this.devices });
  }

  public recap(services: ServiceDTO[]) {
    const categories: CategoryDTO[] = [];
    services.forEach(service => {
      const categoryExist = categories.find(s => s.name === service.category);
      if (categoryExist) {
        categoryExist.services.push(service);
      }
      else {
        const category = new CategoryDTO();
        category.name = service.category;
        category.services = [service];
        categories.push(category);
      }
    });
    return categories;
  }




  /**
   * Инициализирует категории по типу
   * @param respServices Ответ от сервера
   */
  private initCategories(respServices: ServiceDTO[]) {
    respServices.forEach(respService => {
      const categoryExist = this.categories.find(s => s.name === respService.category);
      if (categoryExist) {
        categoryExist.services.push(respService);
      }
      else {
        const category = new CategoryDTO();
        category.name = respService.category;
        category.services = [respService];
        this.categories.push(category);
      }
    });
  }

}
