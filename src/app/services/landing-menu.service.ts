import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LandingMenuService {

  scrollSubject: Subject<HTMLElement> =
    new Subject<HTMLElement>();

  public menuHeight = 0;

  public isTop = true;

  constructor() { }

  public scroll(toElement) {

    this.scrollSubject.next(toElement);

  }

}
