import { Injectable } from "@angular/core";
import { CalculatorAnalytic } from "./analytic-calculator.interface";
import { GoogleAnalyticService } from "./google-analytic.service";
// import { YanexAnalyticService } from "./yanex-analytic.service";

@Injectable({
  providedIn: "root"
})
export class CalculatorAnalyticService implements CalculatorAnalytic {
  constructor(
    private googleAnalytic: GoogleAnalyticService,
    // private yandexAnalytic: YanexAnalyticService
  ) {}

  calculateTheCostClick() {
    this.googleAnalytic.calculateTheCostClick();
    // this.yandexAnalytic.calculateTheCostClick();
  }
  sendCalculationClick() {
    this.googleAnalytic.sendCalculationClick();
    // this.yandexAnalytic.sendCalculationClick();
  }
  viberLinkClick() {
    this.googleAnalytic.viberLinkClick();
    // this.yandexAnalytic.viberLinkClick();
  }
  telegramLinkClick() {
    this.googleAnalytic.telegramLinkClick();
    // this.yandexAnalytic.telegramLinkClick();
  }
  messengerLinkClick() {
    this.googleAnalytic.messengerLinkClick();
    // this.yandexAnalytic.messengerLinkClick();
  }
  vkLinkClick() {
    this.googleAnalytic.vkLinkClick();
    // this.yandexAnalytic.vkLinkClick();
  }
  waLinkClick() {
    this.googleAnalytic.waLinkClick();
    // this.yandexAnalytic.waLinkClick();
  }
}
