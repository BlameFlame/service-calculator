import { Injectable } from "@angular/core";
import { CalculatorAnalytic } from "./analytic-calculator.interface";
import { Metrika } from "ng-yandex-metrika";

@Injectable({
  providedIn: "root"
})
export class YanexAnalyticService implements CalculatorAnalytic {
  public readyToUse() {
    return Boolean((<any>window).yaCounter50086873);
  }

  constructor(private metrika: Metrika) {}

  hit = tag => {
    if (this.readyToUse()) {
      (<any>window).yaCounter50086873.hit(tag);
      this.metrika.hit(tag);
    }
  };

  reachGoal = tag => {
    if (this.readyToUse()) {
      (<any>window).yaCounter50086873.reachGoal(tag);
      this.metrika.fireEvent(tag);
    }
  };

  calculateTheCostClick() {
    this.reachGoal("raschet");
  }
  sendCalculationClick() {
    this.reachGoal("raschetCalc");
  }
  viberLinkClick() {
    this.reachGoal("vb");
  }
  telegramLinkClick() {
    this.reachGoal("tg");
  }
  messengerLinkClick() {
    this.reachGoal("ms");
  }
  vkLinkClick() {
    this.reachGoal("vk");
  }
  waLinkClick() {
    this.reachGoal("wa");
  }
}
