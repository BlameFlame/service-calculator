import { Injectable } from "@angular/core";
import { CalculatorAnalytic } from "./analytic-calculator.interface";

@Injectable({
  providedIn: "root"
})
export class GoogleAnalyticService implements CalculatorAnalytic {
  public readyToUse() {
    return Boolean((<any>window).ga);
  }

  constructor() {}

  sendEvent = (event, label, tag, value) => {
    if (this.readyToUse()) {
      (<any>window).ga("send", "event", {
        eventCategory: event,
        eventLabel: label,
        eventAction: tag,
        eventValue: value
      });
    }
  };

  pageChange = path => {
    if (this.readyToUse()) {
      (<any>window).ga("set", "page", path);
      (<any>window).ga("send", "pageview");
    }
  };

  calculateTheCostClick() {
    this.sendEvent("submit", "submit", "raschet", 0);
  }
  sendCalculationClick() {
    this.sendEvent("submit", "submit", "raschetCalc", 0);
  }
  viberLinkClick() {
    this.sendEvent("click", "click", "vb", 0);
  }
  telegramLinkClick() {
    this.sendEvent("click", "click", "tg", 0);
  }
  messengerLinkClick() {
    this.sendEvent("click", "click", "ms", 0);
  }
  vkLinkClick() {
    this.sendEvent("click", "click", "vk", 0);
  }
  waLinkClick() {
    this.sendEvent("click", "click", "wa", 0);
  }
}
