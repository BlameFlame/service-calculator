export interface CalculatorAnalytic {
  calculateTheCostClick();
  sendCalculationClick();
  viberLinkClick();
  telegramLinkClick();
  messengerLinkClick();
  vkLinkClick();
  waLinkClick();
}
