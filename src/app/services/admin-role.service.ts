import { Injectable } from '@angular/core';
import { ApiService } from '../provider/api/api.service';
import { Router, CanActivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AdminRoleService implements CanActivate {

  constructor(public api: ApiService, public router: Router) { }

  private getOrders() {

  }

  canActivate(): boolean {
    try {
      this.getOrders();
      return true;
    }
    catch{
      return false;
    }
  }

}
