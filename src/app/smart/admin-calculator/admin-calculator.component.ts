import { Component, OnInit, Output, EventEmitter, Input, ViewChild, HostListener, ElementRef, ChangeDetectionStrategy } from '@angular/core';
import { CategoryDTO } from '../../data/dto/categoryDTO';
import { FooterComponent } from '../../dumb/footer/footer.component';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { DeviceDTO } from '../../data/dto/deviceDTO';

@Component({
  selector: 'app-admin-calculator',
  templateUrl: './admin-calculator.component.html',
  styleUrls: ['./admin-calculator.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('flyInOut', [
      state('in', style({ opacity: 1 })),
      transition('void => *', [
        style({ opacity: 0 }),
        animate('300ms ease-in')
      ]),
      transition('* => void', [
        animate('300ms ease-out', style({ opacity: 0 }))
      ])
    ])
  ]
})
export class AdminCalculatorComponent implements OnInit {

  private _phoneNum = '';
  private _email = '';
  private _categories: CategoryDTO[] = [];
  private _note = '';

  private _initNote = '';

  @Output()
  public onAccept = new EventEmitter<void>();

  @Output()
  public onCancel = new EventEmitter<void>();

  @Output()
  public phoneNumberChange = new EventEmitter<string>();

  @Output()
  public emailChange = new EventEmitter<string>();

  @Output()
  public noteChange = new EventEmitter<string>();

  @Input()
  public set phoneNumber(value) {
    if (this._phoneNum !== value) {
      this._phoneNum = value;
      this.phoneNumberChange.next(value);
    }
  }
  public get phoneNumber() {
    return this._phoneNum;
  }

  @Input()
  public set email(value) {
    if (this._email !== value) {
      this._email = value;
      this.emailChange.next(value);
    }
  }
  public get email() {
    return this._email;
  }

  @Input()
  public get categories() {
    return this._categories;
  }
  public set categories(value) {
    this._categories = value;
  }

  @Input()
  public get note() {
    return this._note;
  }
  public set note(value) {
    if (this._note !== value) {
      this._note = value;
      this.noteChange.emit(value);
    }
  }

  public get orderDevices() {
    const devices: DeviceDTO[] = [];
    this.categories.forEach(category => {
      category.services.forEach(service => {
        service.devices.forEach(device => {
          if (device.count > 0) {
            devices.push(device);
          }
        });
      });
    });
    return devices;
  }

  public isActionFormFixed = true;

  @ViewChild(FooterComponent) footer: FooterComponent;

  @HostListener('scroll', ['$event']) onscroll(event) {
    this.updateActionPositionUpdate();
  }



  constructor(public elementRef: ElementRef) { }

  /**
   * Обновить отображение формы в зависимости от положения скрола
   */
  private updateActionPositionUpdate() {
    const scrollTopCalc = this.elementRef.nativeElement.scrollTop;
    const heightCaclc = this.elementRef.nativeElement.offsetHeight;
    const offsetBottom = scrollTopCalc + heightCaclc;

    const scrollTopFooter = this.footer.nativeElement.offsetTop;

    if (offsetBottom >= scrollTopFooter) {
      this.isActionFormFixed = false;
    }
    else {
      this.isActionFormFixed = true;
    }
  }


  public accept() {
    this.onAccept.next();
  }

  public close() {
    this.onCancel.next();
  }

  ngOnInit() {
  }
}
