import { Component, OnInit, OnDestroy, AfterViewInit, AfterContentInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../provider/api/api.service';
import { CategoryDTO } from '../../data/dto/categoryDTO';
import { CalculatorService } from '../../services/calculator.service';
import { OrderStatus } from '../../data/enums/order-status';
import { Location } from '@angular/common';
import { DeviceDTO } from '../../data/dto/deviceDTO';
import { UserService } from '../../services/user.service';
import { ServiceDTO } from '../../data/dto/serviceDTO';
import { RequestStatus } from '../../data/enums/request-status';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit, AfterViewInit, AfterContentInit {

  public requestStatus = RequestStatus.loading;

  public orderNumber = 1337;
  public orderDate: Date = new Date();
  public categories: CategoryDTO[] = [];
  public services: ServiceDTO[] = [];
  public orderStatus: OrderStatus;
  public phoneNumber: string;
  public email: string;
  public orderId: string;
  public note: string;

  public get isAdmin() {
    return Boolean(this.userService.getToken());
  }

  constructor(
    private api: ApiService,
    private route: ActivatedRoute,
    private location: Location,
    private calcService: CalculatorService,
    private userService: UserService) { }

  ngAfterContentInit(): void {
    this.orderId = this.route.snapshot.paramMap.get('id');
    this.api.getOrder(this.orderId).subscribe(
      resp => {
        this.services = resp.serviceResponces;
        this.categories = this.calcService.recap(resp.serviceResponces);
        this.orderStatus = resp.orderStatus;
        this.phoneNumber = resp.phoneNumber;
        this.orderDate = resp.orderDate;
        this.orderNumber = resp.orderNumber;
        this.email = resp.email;
        this.note = resp.note;
      }
    );
  }

  ngAfterViewInit(): void {

  }

  ngOnInit() {

  }

  public updateOrder(devices: DeviceDTO[]) {
    this.api.updateOrder(this.orderId, this.phoneNumber, this.orderStatus, devices, this.note).subscribe(
      {
        next: resp => { console.log('Заказ/Расчет успешно изменен.'); this.requestStatus = RequestStatus.success; },
        error: error => { console.log('Произошла ошибка при изменении Заказа/Расчета.', error); this.requestStatus = RequestStatus.error; }
      }
    );
  }

  public calculatorClose() {
    this.location.back();
  }

}
