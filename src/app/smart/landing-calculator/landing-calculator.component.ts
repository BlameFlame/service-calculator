import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  ViewChild,
  HostListener,
  ElementRef,
  ChangeDetectionStrategy
} from "@angular/core";
import {
  trigger,
  state,
  style,
  transition,
  animate
} from "@angular/animations";
import { FooterComponent } from "../../dumb/footer/footer.component";
import { CategoryDTO } from "../../data/dto/categoryDTO";
import { FormGroup, FormBuilder } from "@angular/forms";
import { CalculatorService } from "../../services/calculator.service";

@Component({
  selector: "app-landing-calculator",
  templateUrl: "./landing-calculator.component.html",
  styleUrls: ["./landing-calculator.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger("flyInOut", [
      state("in", style({ opacity: 1 })),
      transition("void => *", [
        style({ opacity: 0 }),
        animate("300ms ease-in")
      ]),
      transition("* => void", [
        animate("300ms ease-out", style({ opacity: 0 }))
      ])
    ])
  ]
})
export class LandingCalculatorComponent implements OnInit {
  private _phoneNum = "";
  private _email = "";
  private _categories: CategoryDTO[] = [];

  @Output()
  public onAccept = new EventEmitter<void>();

  @Output()
  public onCancel = new EventEmitter<void>();

  @Output()
  public phoneNumberChange = new EventEmitter<string>();

  @Output()
  public emailChange = new EventEmitter<string>();

  @Input()
  public set phoneNumber(value) {
    this._phoneNum = value;
    this.serviceCalc.phoneNumber = value;
  }
  public get phoneNumber() {
    return this._phoneNum;
  }

  @Input()
  public set email(value) {
    if (this._email !== value) {
      this._email = value;
      this.emailChange.next(value);
    }
  }
  public get email() {
    return this._email;
  }

  @Input()
  public get categories() {
    return this._categories;
  }
  public set categories(value) {
    this._categories = value;
  }

  @Input()
  public isAdmin = false;

  public isActionFormFixed = true;
  public totalPrice = 0;

  @ViewChild(FooterComponent)
  footer: FooterComponent;
  @ViewChild("footerPlace")
  footerPlace: FooterComponent;

  @HostListener("scroll", ["$event"])
  onscroll(event) {
    const scrollTopCalc = this.elementRef.nativeElement.scrollTop;
    const heightCaclc = this.elementRef.nativeElement.offsetHeight;
    const offsetBottom = scrollTopCalc + heightCaclc;

    const scrollTopFooter = this.footer.nativeElement.offsetTop;

    if (offsetBottom >= scrollTopFooter) {
      this.isActionFormFixed = false;
    } else {
      this.isActionFormFixed = true;
    }
  }

  complexForm: FormGroup;

  constructor(
    public elementRef: ElementRef,
    fb: FormBuilder,
    private serviceCalc: CalculatorService
  ) {
    this.complexForm = fb.group({
      phoneNumber: this.phoneNumber
    });

    this.complexForm.valueChanges.subscribe(value => {
      this.phoneNumber = value.phoneNumber;
    });
  }

  totalPriceChangeHandler(value: number) {
    this.totalPrice = value;
    this.serviceCalc.totalPrice = value;
  }

  public accept() {
    this.onAccept.next();
  }

  public close() {
    this.onCancel.next();
  }

  ngOnInit() {}
}
