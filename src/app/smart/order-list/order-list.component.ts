import { Component, OnInit, Input } from '@angular/core';
import { ServiceDTO } from '../../data/dto/serviceDTO';
import { DeviceType } from '../../data/enums/device-type';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {

  /**
   * Дата заказа
   */
  @Input()
  public orderDate: Date = new Date();

  /**
   * Номер заказа
   */
  @Input()
  public orderNumber = 1337;

  /**
   * Описание заказа от Администратора
   */
  @Input()
  public note = '';

  @Input()
  public services: ServiceDTO[] = [];

  public get totalPrice() {
    return this.services.reduce((a, b) => a + this.calcTotalServicePrice(b), 0);
  }

  constructor() { }

  ngOnInit() {
  }

  public isServiceHasMkad(service: ServiceDTO) {
    return Boolean(service.devices.find(d => d.type === DeviceType.mkadOut));
  }

  public calcTotalServicePrice(service: ServiceDTO) {
    return service.devices.reduce((a, b) => a + b.price * b.count, 0);
  }

  public isEmptyService(service: ServiceDTO) {
    return !Boolean(service.devices.find(d => d.count !== 0));
  }

  public serviceDevicesTypeCount(service: ServiceDTO) {
    return service.devices.reduce((a, b) => a + b.count !== 0 ? 1 : 0, 0);
  }
}
