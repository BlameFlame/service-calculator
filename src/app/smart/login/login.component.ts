import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../provider/api/api.service';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginValue = '';
  public passwordValue = '';

  constructor(private api: ApiService, private userService: UserService, private router: Router) { }

  ngOnInit() {
  }

  public login() {
    this.api.login(this.loginValue, this.passwordValue).subscribe(
      (response) => {
        this.userService.setToken(response.token);
        this.router.navigate(['/admin']);
      }
    );
  }

}
