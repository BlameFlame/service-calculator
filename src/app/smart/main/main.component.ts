import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "../../provider/api/api.service";
import { CalculatorService } from "../../services/calculator.service";
import { ModalService } from "../../services/modal.service";
import { CategoryDTO } from "../../data/dto/categoryDTO";
import { Subscription } from "rxjs";
import { CalculatorState } from "../../data/enums/claculator-state";
import { OrderStatus } from "../../data/enums/order-status";
import { RequestStatus } from "../../data/enums/request-status";
import { CalculatorAnalyticService } from "../../services/analytics/calculator-analytic.service";

@Component({
  selector: "app-main",
  templateUrl: "./main.component.html",
  styleUrls: ["./main.component.scss"]
})
export class MainComponent implements OnInit, OnDestroy {
  private calculateServiceInitSubscription: Subscription;

  public categories: CategoryDTO[] = [];
  public calculatorState: CalculatorState = CalculatorState.inactive;

  public requestStatus = RequestStatus.loading;
  public createdOrderId: string;

  public get orderStatus() {
    return this.calcService.orderStatus;
  }
  public set orderStatus(value) {
    this.calcService.orderStatus = value;
  }

  public get isActive() {
    return this.calculatorState === CalculatorState.active;
  }

  public get phoneNumber() {
    return this.calcService.phoneNumber;
  }

  public set phoneNumber(value) {
    this.calcService.phoneNumber = value;
  }

  constructor(
    private api: ApiService,
    private calcService: CalculatorService,
    private modalService: ModalService,
    private router: Router,
    private calculatorAnalytic: CalculatorAnalyticService
  ) {
    this.calculateServiceInitSubscription = this.calcService.onInit.subscribe(
      () => (this.categories = this.calcService.categories)
    );

    this.calcService.stateChange.subscribe(
      newState => (this.calculatorState = newState)
    );

    this.router.events.subscribe(value => {
      this.calcService.close();
    });
  }

  ngOnInit() {
    // this.api.getSevices().subscribe(
    //   {
    //     next: servicesResponse => { this.calcService.initServices(servicesResponse); this.requestDialog.success(); },
    //     error: error => { console.error('Не удалось получить сервисы с базы данных.', error); this.requestDialog.success(); }
    //   }
    // );
    this.api.getSevices().subscribe({
      next: servicesResponse => {
        this.calcService.initServices(servicesResponse);
      },
      error: error => {
        console.error("Не удалось получить сервисы с базы данных.", error);
      }
    });
  }

  ngOnDestroy() {
    this.calculateServiceInitSubscription.unsubscribe();
  }

  public calculatorClose() {
    this.calcService.close();
  }

  public calculatorClear() {
    this.calcService.clear();
  }

  public modalClosed() {
    this.requestStatus = RequestStatus.loading;
  }

  public navigateToOreder() {
    this.router.navigate([
      "/",
      "admin",
      "orders",
      "order",
      this.createdOrderId
    ]);
  }

  private createOrder() {
    const app = this;

    const createOrderObserver = this.api.createOrder(
      this.phoneNumber,
      this.orderStatus,
      this.calcService.getDevicesWithValue()
    );

    createOrderObserver.subscribe({
      next(res) {
        console.log("Заказ успешно создан");
        app.requestStatus = RequestStatus.success;
        app.createdOrderId = res;
        app.calculatorAnalytic.sendCalculationClick();
      },
      error(msg) {
        if (msg.error.orderId) {
          app.createdOrderId = msg.error.orderId;
          app.requestStatus = RequestStatus.success;
        } else {
          console.error("Ошибка при создании заказа");
          app.requestStatus = RequestStatus.error;
        }
      }
    });
  }

  public emptyOrder() {
    this.modalService.open("ty-empty");
    this.createOrder();
  }

  public notEmptyOrder() {
    this.modalService.open("ty-not-empty");
    this.createOrder();
  }
}
