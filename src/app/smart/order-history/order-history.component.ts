import { OrderHistoryDTO } from '../../data/dto/order-historyDTO';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../provider/api/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.css']
})
export class OrderHistoryComponent implements OnInit {
  public orderId: string;
  public orderHistory: OrderHistoryDTO[] = [];

  constructor(public api: ApiService, private route: ActivatedRoute, private location: Location) { }

  ngOnInit() {
    this.orderId = this.route.snapshot.paramMap.get('id');
    this.api.getOrderHistory(this.orderId).subscribe(
      orderHistory => this.orderHistory = orderHistory,
      error => console.error(`Не удалось получить историю заказов от сервера.`, error)
    );
  }

  public back() {
    this.location.back();
  }
}
