import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../provider/api/api.service';
import { UserService } from '../../services/user.service';
import { OrderDTO } from '../../data/dto/orderDTO';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  public orders: OrderDTO[] = [];

  public orderStateToRu = {
    Calculation: 'Расчет',
    Order: 'Заказ'
  };

  public startIndex = 0;
  public displayOrders = 10;

  constructor(public router: Router, private api: ApiService, private user: UserService) { }

  ngOnInit() {
    this.getOrders();
  }

  public getOrders() {
    this.api.getOrders(this.startIndex, this.displayOrders).subscribe(
      (orders) => { this.orders = orders; },
      error => console.error(`Не удалось получить заказы от сервера.`, error),
    );
  }

  private jumpIndex(index: number) {
    const newIndex = Math.max(this.startIndex + index, 0);
    this.startIndex = newIndex;
    this.api.getOrders(this.startIndex, this.displayOrders).subscribe(
      (orders) => { this.orders = orders; }
    );
  }

  public pageBack() {
    this.jumpIndex(-this.displayOrders);
  }

  public pageFront() {
    if (this.orders.length >= 10) {
      this.jumpIndex(this.displayOrders);
    }
  }

}
