import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  Output,
  EventEmitter,
  HostListener,
  ContentChild,
  ViewChild,
  ElementRef,
  ChangeDetectionStrategy
} from "@angular/core";
import {
  trigger,
  state,
  style,
  animate,
  transition
} from "@angular/animations";
import { CategoryDTO } from "../../data/dto/categoryDTO";
import { DeviceDTO } from "../../data/dto/deviceDTO";
import { ServiceDTO } from "../../data/dto/serviceDTO";
import { DeviceType } from "../../data/enums/device-type";
import { OrderStatus } from "../../data/enums/order-status";
import { FooterComponent } from "../../dumb/footer/footer.component";
import { CalculatorAnalyticService } from "../../services/analytics/calculator-analytic.service";

@Component({
  selector: "app-calculator",
  templateUrl: "./calculator.component.html",
  styleUrls: ["./calculator.component.scss"]
})
export class CalculatorComponent implements OnInit, OnDestroy {
  private _categories: CategoryDTO[] = [];
  private _totalPrice = 0;

  @Output()
  totalPriceChange: EventEmitter<number> = new EventEmitter<number>();

  @Input()
  public get categories() {
    return this._categories;
  }
  public set categories(value) {
    this._categories = value;
  }

  public get devices() {
    let res: DeviceDTO[] = [];

    this.services.forEach(service => {
      res = res.concat(service.devices);
    });

    return res;
  }

  public get services() {
    let res: ServiceDTO[] = [];

    this.categories.forEach(category => {
      res = res.concat(category.services);
    });

    return res;
  }

  public get orderDevices() {
    const devices: DeviceDTO[] = [];
    this.categories.forEach(category => {
      category.services.forEach(service => {
        service.devices.forEach(device => {
          if (device.count > 0) {
            devices.push(device);
          }
        });
      });
    });
    return devices;
  }

  public get isEmpty() {
    const valuableDevoces = this.orderDevices.filter(
      od => od.type !== DeviceType.mkadOut && od.type !== DeviceType.smarthub
    );
    return valuableDevoces.length === 0;
  }

  @Input()
  public get totalPrice() {
    return this._totalPrice;
  }
  public set totalPrice(value) {
    if (value !== this._totalPrice) {
      this._totalPrice = value;
      this.totalPriceChange.emit(value);
    }
  }

  public get requiredSmartHubDevices() {
    return this.orderDevices.filter(d => d.requiredSmartHub).length;
  }

  constructor(
    public elementRef: ElementRef,
    private calcAnatytic: CalculatorAnalyticService
  ) {}

  public ngOnInit() {
    this.calcAnatytic.calculateTheCostClick();
  }

  public ngOnDestroy() {}

  /**
   * Метод вызывающийся при отмене выбора сервиса
   * @param service сервис
   */
  public serviceDeactivated(service: ServiceDTO) {}

  /**
   * Метод вызывающийся при выборе сервиса
   * @param service сервис
   */
  public serviceActivated(service: ServiceDTO) {}

  public isServiceEmpty(service: ServiceDTO): boolean {
    return Boolean(service.devices.find(d => d.count !== 0));
  }

  private isServiceHasDeviceType(service: ServiceDTO, type: DeviceType) {
    const smartHubDevice = service.devices.find(d => d.type === type);
    return Boolean(smartHubDevice);
  }

  public isServiceHasSmartHub(service: ServiceDTO): boolean {
    return this.isServiceHasDeviceType(service, DeviceType.smarthub);
  }

  public isServiceHasMkad(service: ServiceDTO): boolean {
    return this.isServiceHasDeviceType(service, DeviceType.mkadOut);
  }

  public isCategoryDeviceType(category: CategoryDTO, type: DeviceType) {
    let typedDevice = 0;
    category.services.forEach(service => {
      if (this.isServiceHasDeviceType(service, type)) {
        typedDevice++;
      }
    });
    return typedDevice !== 0;
  }

  public isCategoryEmpty(category: CategoryDTO) {
    let countedDevice = 0;
    category.services.forEach(service => {
      if (this.isServiceEmpty(service)) {
        countedDevice++;
      }
    });
    return countedDevice !== 0;
  }

  public isCategoryHasMkad(category: CategoryDTO) {
    return this.isCategoryDeviceType(category, DeviceType.mkadOut);
  }

  public isCategoryHasSmartHub(category: CategoryDTO) {
    return this.isCategoryDeviceType(category, DeviceType.smarthub);
  }

  /**
   * Метод вызываемы при изменении цены устройства
   * @param event Эвент изменения общей цены
   * @param device Устройство
   */
  public deviceTotalPriceChanged(event: {
    oldValue: number;
    newValue: number;
  }) {
    // TO DO как то подругому надо
    setTimeout(() => {
      this.totalPrice -= event.oldValue;
      this.totalPrice += event.newValue;
    }, 0);
  }

  /**
   * Метод вызываемый при изменении количесва устройств
   * @param event Эвент изменения количества устройства
   * @param device Устройство
   */
  public deviceCountChanged(
    event: { oldValue: number; newValue: number },
    device: DeviceDTO
  ) {
    const oldValue = event.oldValue;
    const newValue = event.newValue;

    // Если устройство должно иметь смартхаб
    if (device.requiredSmartHub) {
      // Поиск из всех устройств смартхаба
      const smartHub = this.devices.find(d => d.type === DeviceType.smarthub);

      // Поиск всех устройст (Исключая изменяемое), количество которых > 0 и устройство должно иметь смартхаб
      const reqSmarthubDevices = this.orderDevices.filter(
        d => d.id !== device.id && d.requiredSmartHub
      ).length;

      // Если устройств нет, то рассматривается изменение изменяемого устройства
      if (reqSmarthubDevices === 0) {
        // если количество изменяемого устройства становится 0, то смарт хаб не нужен
        if (newValue === 0) {
          smartHub.count = 0;
        }
        // если количество изменяемого устройства становится 1, то смарт хаб нужен
        if (newValue === 1 && oldValue === 0) {
          smartHub.count = 1;
        }
      }
    }
  }
}
