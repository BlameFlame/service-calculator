import { NgModule } from '@angular/core';
import { ApiService } from './api/api.service';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
    declarations: [
    ],
    exports: [
        HttpClientModule
    ],
    imports: [
        HttpClientModule
    ],
    providers: [],
})
export class ProviderModule { }
