import { OrderHistoryDTO } from '../../data/dto/order-historyDTO';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ServiceDTO } from '../../data/dto/serviceDTO';
import { UserService } from '../../services/user.service';
import { DeviceDTO } from '../../data/dto/deviceDTO';
import { OrderDTO } from '../../data/dto/orderDTO';
import { Observable, Subject } from 'rxjs';
import { environment } from '../../../environments/environment';
import { OrderStatus } from '../../data/enums/order-status';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  // public API_HOST = 'http://localhost:3004/';
  // public API_HOST = 'http://212.104.72.184:13000/api/v1/';
  public API_HOST = environment.API_HOST;

  constructor(private http: HttpClient, private userService: UserService) { }

  /**
   * Получить все сервисы
   */
  public getSevices() {
    const obs = this.http.get<ServiceDTO[]>(this.API_HOST + 'services');

    const f = new Observable<ServiceDTO[]>(
      (observer) => {
        let services: ServiceDTO[] = [];
        obs.subscribe(
          resp => {
            services = resp;
            services.forEach(
              service => service.devices.forEach(device => {
                device.serviceId = service.id;
                device.count === undefined ? device.count = 0 : device.count = device.count;
              })
            );
            observer.next(services);
            observer.complete();
          }
        );

      }
    );


    return f;
  }

  public login(login: string, password: string) {
    const body = {
      login: login,
      password: password
    };
    return this.http.post<{ token: string }>(this.API_HOST + 'Account/login', body);
  }

  public getOrders(startindex: number = 0, count: number = 10) {
    let params = new HttpParams();
    params = params.append('startindex', startindex.toString());
    params = params.append('count', count.toString());

    const options = {
      params: params
    };

    return this.http.post<OrderDTO[]>(this.API_HOST + 'Orders/loadOrders', {}, options);
  }

  public createOrder(phoneNumber: string, orderStatus: OrderStatus, devices: DeviceDTO[], email?: string, ) {
    const body = {
      phoneNumber: phoneNumber,
      orderStatus: orderStatus,
      devices: devices,
      email: email,
    };



    return this.http.post<string>(this.API_HOST + 'Orders', body);
  }

  public getOrder(id: string) {
    interface ResponseType {
      id: string;
      orderDate: Date;
      orderNumber: number;
      phoneNumber: string;
      email: string;
      orderStatus: OrderStatus;
      serviceResponces: ServiceDTO[];
      note: string;
    }

    const obs = this.http.get<ResponseType>(this.API_HOST + 'Orders/' + id);

    const f = new Observable<ResponseType>(
      (observer) => {
        let services: ServiceDTO[] = [];
        obs.subscribe(
          resp => {
            services = resp.serviceResponces;
            services.forEach(
              service => service.devices.forEach(device => device.serviceId = service.id)
            );
            observer.next(resp);
            observer.complete();
          }
        );

      }
    );

    return f;
  }

  public updateOrder(id: string, phoneNumber: string, orderStatus: OrderStatus, devices: DeviceDTO[], note?: string, email?: string) {

    const body = {
      phoneNumber: phoneNumber,
      orderStatus: orderStatus,
      email: email,
      devices: devices,
      note: note
    };

    return this.http.put(this.API_HOST + 'Orders/' + id, body);
  }

  public getOrderHistory(id: string) {
    return this.http.get<OrderHistoryDTO[]>(this.API_HOST + 'Orders/history/' + id);
  }


}
